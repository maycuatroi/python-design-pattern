from Entities.Computer import Computer


class PC(Computer):
    def __init__(self, ram, hdd, cpu):
        self.ram = ram
        self.cpu = cpu
        self.hdd = hdd

    def get_ram(self) -> str:
        return self.ram

    def get_hdd(self) -> str:
        return self.cpu

    def get_cpu(self) -> str:
        return self.hdd
