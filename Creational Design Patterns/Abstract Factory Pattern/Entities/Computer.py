import abc


class Computer(object, metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def get_ram(self) -> str:
        raise NotImplementedError("Should have implemented this")

    @abc.abstractmethod
    def get_hdd(self) -> str:
        raise NotImplementedError("Should have implemented this")

    @abc.abstractmethod
    def get_cpu(self) -> str:
        raise NotImplementedError("Should have implemented this")

    def __str__(self) -> str:
        return "RAM= " + self.get_ram() + ", HDD=" + self.get_hdd() + ", CPU=" + self.get_cpu()
