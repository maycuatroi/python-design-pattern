from Entities.Computer import Computer


class Server(Computer):
    def __init__(self, ram, hdd, cpu, ssh_key):
        self.ram = ram
        self.cpu = cpu
        self.hdd = hdd
        self.ssh_key = ssh_key

    def get_ram(self) -> str:
        return self.ram

    def get_hdd(self) -> str:
        return self.cpu

    def get_cpu(self) -> str:
        return self.hdd
