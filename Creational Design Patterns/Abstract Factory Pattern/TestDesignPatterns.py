from Controler.ComputerFactory import ComputerFactory
from Controler.PCFactory import PCFactory
from Controler.ServerFactory import ServerFactory

pc = ComputerFactory.get_computer(PCFactory("2 GB", "500 GB", "2.4 GHz"))
print("PC : \t", pc)

print()
print()

server1 = ComputerFactory.get_computer(ServerFactory("16 GB", "1 TB", "2.9 GHz"))
print("Server 1 : \t", server1)
print()
server2 = ComputerFactory.get_computer(ServerFactory("8 GB", "1 TB", "2.9 GHz"))
print("Server 1 : \t", server2)
